﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using AutoMapper;
using CourseLibrary.API.ActionConstraints;
using CourseLibrary.API.Entities;
using CourseLibrary.API.Helpers;
using CourseLibrary.API.Models;
using CourseLibrary.API.ResourceParameters;
using CourseLibrary.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace CourseLibrary.API.Controllers
{
    [ApiController]
    [Route("api/authors")]
    public class AuthorsController : ControllerBase
    {
        private readonly ICourseLibraryRepository _repository;
        private readonly IMapper _mapper;

        public AuthorsController(ICourseLibraryRepository courseLibraryRepository, IMapper mapper)
        {
            _repository = courseLibraryRepository ?? throw new ArgumentNullException(nameof(courseLibraryRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetAuthors")]
        [HttpHead]
        public IActionResult GetAuthors([FromQuery] AuthorsResourceParameters parameters,
            [FromServices] IPropertyMappingService propertyMappingService,
            [FromServices] IPropertyCheckerService propertyCheckerService)
        {
            if (propertyMappingService == null) throw new ArgumentNullException(nameof(propertyMappingService));
            if (propertyCheckerService == null) throw new ArgumentNullException(nameof(propertyCheckerService));

            if (!propertyMappingService.ValidMappingExistsFor<AuthorDto, Author>(parameters.OrderBy))
            {
                return BadRequest();
            }

            if (!propertyCheckerService.TypeHasProperties<AuthorDto>(parameters.Fields))
            {
                return BadRequest();
            }

            var authors = _repository.GetAuthors(parameters);

            var paginationMetadata = new
            {
                authors.TotalCount,
                authors.PageSize,
                authors.CurrentPage,
                authors.TotalPages
            };

            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata,
                new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase }));

            var links = CreateLinksForAuthors(parameters, authors.HasNext, authors.HasPrevious);
            var shapedAuthors = _mapper.Map<IEnumerable<AuthorDto>>(authors)
                .ShapeData(parameters.Fields);

            var shapedAuthorsWithLinks = shapedAuthors.Select(author =>
            {
                var authorAsDictionary = author as IDictionary<string, object>;
                var authorLinks = CreateLinksForAuthor((Guid)authorAsDictionary["Id"], null);
                authorAsDictionary.Add("Links", authorLinks);
                return authorAsDictionary;
            });

            var linkedCollectionResource = new { value = shapedAuthorsWithLinks, links };

            return Ok(linkedCollectionResource);
        }

        [HttpGet("{authorId:guid}", Name = "GetAuthor")]
        [Produces("application/json",
            "application/vnd.marvin.author.full+json",
            "application/vnd.marvin.author.full.hateoas+json",
            "application/vnd.marvin.author.friendly+json",
            "application/vnd.marvin.author.friendly.hateoas+json")]
        public IActionResult GetAuthor(Guid authorId, string fields,
            [FromHeader(Name = "Accept")] string mediaType,
            [FromServices] IPropertyCheckerService propertyCheckerService)
        {
            if (!MediaTypeHeaderValue.TryParse(mediaType, out var parsedMediaType))
            {
                return BadRequest();
            }

            if (!propertyCheckerService.TypeHasProperties<AuthorDto>(fields))
            {
                return BadRequest();
            }

            var author = _repository.GetAuthor(authorId);

            if (author == null)
            {
                return NotFound();
            }

            var includeLinks = parsedMediaType.SubTypeWithoutSuffix
                .EndsWith("hateoas", StringComparison.InvariantCultureIgnoreCase);

            var primaryMediaType = includeLinks
                ? parsedMediaType.SubTypeWithoutSuffix
                    .Substring(0, parsedMediaType.SubTypeWithoutSuffix.Length - 8)
                : parsedMediaType.SubTypeWithoutSuffix;

            if (primaryMediaType == "vnd.marvin.author.full")
            {
                // Full author
                var fullResourceToReturn = _mapper.Map<AuthorFullDto>(author)
                    .ShapeData(fields) as IDictionary<string, object>;

                if (includeLinks)
                {
                    var links = CreateLinksForAuthor(authorId, fields);
                    fullResourceToReturn.Add("Links", links);
                }

                return Ok(fullResourceToReturn);
            }
            else
            {
                // Friendly author
                var friendlyResourceToReturn = _mapper.Map<AuthorDto>(author)
                    .ShapeData(fields) as IDictionary<string, object>;

                if (includeLinks)
                {
                    var links = CreateLinksForAuthor(authorId, fields);
                    friendlyResourceToReturn.Add("Links", links);
                }

                return Ok(friendlyResourceToReturn);
            }
        }

        [HttpPost(Name = "CreateAuthorWithDateOfDeath")]
        [RequestHeaderMatchesMediaType("Content-Type",
            "application/vnd.marvin.authorforcreationwithdateofdeath+json")]
        [Consumes("application/vnd.marvin.authorforcreationwithdateofdeath+json")]
        public IActionResult CreateAuthorWithDateOfDeath(AuthorForCreationWithDateOfDeathDto authorForCreation)
        {
            var author = _mapper.Map<Author>(authorForCreation);
            _repository.AddAuthor(author);
            _repository.Save();

            var authorToReturn = _mapper.Map<AuthorDto>(author);
            var links = CreateLinksForAuthor(author.Id, null);
            var linkedResourceToReturn = authorToReturn.ShapeData(null) as IDictionary<string, object>;

            linkedResourceToReturn.Add("Links", links);

            return CreatedAtRoute("GetAuthor",
                new { authorId = linkedResourceToReturn["Id"] },
                linkedResourceToReturn);
        }

        [HttpPost(Name = "CreateAuthor")]
        [RequestHeaderMatchesMediaType("Content-Type", "application/json",
            "application/vnd.marvin.authorforcreation+json")]
        [Consumes("application/json", "application/vnd.marvin.authorforcreation+json")]
        public ActionResult<AuthorDto> CreateAuthor(AuthorForCreationDto authorForCreation)
        {
            var author = _mapper.Map<Author>(authorForCreation);
            _repository.AddAuthor(author);
            _repository.Save();

            var authorToReturn = _mapper.Map<AuthorDto>(author);
            var links = CreateLinksForAuthor(authorToReturn.Id, null);
            var linkedResourceToReturn = authorToReturn
                .ShapeData(null) as IDictionary<string, object>;
            linkedResourceToReturn.Add("Links", links);

            return CreatedAtRoute("GetAuthor",
                new { authorId = linkedResourceToReturn["Id"] },
                linkedResourceToReturn);
        }

        [HttpOptions]
        public IActionResult GetAuthorsOptions()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS,POST,DELETE");
            return Ok();
        }

        [HttpDelete("{authorId}", Name = "DeleteAuthor")]
        public ActionResult DeleteAuthor(Guid authorId)
        {
            var authorFromRepo = _repository.GetAuthor(authorId);

            if (authorFromRepo == null)
            {
                return NotFound();
            }

            _repository.DeleteAuthor(authorFromRepo);
            _repository.Save();

            return NoContent();
        }

        private IEnumerable<LinkDto> CreateLinksForAuthor(Guid authorId, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(new LinkDto(Url.Link("GetAuthor", new { authorId }),
                    "self", "GET"));
            }
            else
            {
                links.Add(new LinkDto(Url.Link("GetAuthor", new { authorId, fields }),
                    "self", "GET"));
            }

            links.Add(new LinkDto(Url.Link("DeleteAuthor", new { authorId }),
                "delete_author", "DELETE"));

            links.Add(new LinkDto(Url.Link("CreateCourseForAuthor", new { authorId }),
                "create_course_for_author", "POST"));

            links.Add(new LinkDto(Url.Link("GetCoursesForAuthor", new { authorId }),
                "courses", "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForAuthors(AuthorsResourceParameters parameters,
            bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(new LinkDto(CreateAuthorsResourceUri(parameters, ResourceUriType.Current),
                "self", "GET"));

            if (hasNext)
            {
                links.Add(new LinkDto(CreateAuthorsResourceUri(parameters, ResourceUriType.NextPage),
                    "next-page", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateAuthorsResourceUri(parameters, ResourceUriType.PreviousPage),
                    "previous-page", "GET"));
            }

            return links;
        }

        private string CreateAuthorsResourceUri(AuthorsResourceParameters parameters, ResourceUriType type)
        {
            int pageNumber;

            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    pageNumber = parameters.PageNumber - 1;
                    break;
                case ResourceUriType.NextPage:
                    pageNumber = parameters.PageNumber + 1;
                    break;
                default:
                    pageNumber = parameters.PageNumber;
                    break;
            }

            return Url.Link("GetAuthors",
                new
                {
                    pageNumber,
                    pageSize = parameters.PageSize,
                    mainCategory = parameters.MainCategory,
                    searchQuery = parameters.SearchQuery,
                    orderBy = parameters.OrderBy,
                    fields = parameters.Fields
                });
        }
    }
}
