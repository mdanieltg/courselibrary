﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using CourseLibrary.API.Services;

namespace CourseLibrary.API.Helpers
{
    public static class IQueryableExtensions
    {
        private const string Separator = ", ";
        private const string Descending = " descending";
        private const string Ascending = " ascending";

        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, string orderBy,
            Dictionary<string, PropertyMappingValue> mappingDictionary)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (mappingDictionary == null) throw new ArgumentNullException(nameof(mappingDictionary));

            if (string.IsNullOrWhiteSpace(orderBy))
            {
                return source;
            }

            var orderByString = string.Empty;
            // var orderby = new StringBuilder();

            // The orderBy string is separated by ",", so we split it
            var orderByAfterSplit = orderBy.Split(',');

            // Apply each orderBy clause
            foreach (var orderByClause in orderByAfterSplit)
            {
                // Trim the OderBy clause, as it might contain leading or trailing spaces.
                // Can't trim the var in foreach, so use another var
                var trimmedOrderByClause = orderByClause.Trim();

                // If the sort option ends with " desc", we sort descending, ascending otherwise
                var orderDescending = trimmedOrderByClause.EndsWith(" desc");

                // Remove " asc" or " desc" from the orderBy clause, so we get the property name to look for
                // in the mapping dictionary
                var indexOfFirstSpace = trimmedOrderByClause.IndexOf(' ');
                var propertyName = indexOfFirstSpace == -1
                    ? trimmedOrderByClause
                    : trimmedOrderByClause.Remove(indexOfFirstSpace);

                // Try to get the matching property
                if (!mappingDictionary.TryGetValue(propertyName, out var propertyMappingValue))
                {
                    throw new ArgumentException($"Key mapping for {propertyName} is missing");
                }

                if (propertyMappingValue == null)
                {
                    throw new ArgumentNullException(nameof(propertyMappingValue));
                }

                // Revert sort order if necessary
                if (propertyMappingValue.Revert)
                {
                    orderDescending = !orderDescending;
                }

                // Iterate through property names so the orderBy clauses are applied in the correct order
                foreach (var destinationProperty in propertyMappingValue.DestinationProperties)
                {
                    orderByString = orderByString +
                                    (string.IsNullOrWhiteSpace(orderByString) ? string.Empty : ", ")
                                    + destinationProperty
                                    + (orderDescending ? " descending" : " ascending");

                    // orderby.AppendFormat("{0}{1}{2}",
                    //     string.IsNullOrWhiteSpace(orderByString) ? string.Empty : Separator,
                    //     destinationProperty,
                    //     orderDescending ? Descending : Ascending);
                }
            }

            return source.OrderBy(orderByString);
        }
    }
}
