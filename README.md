# CourseLibrary

Cursos:
- Building a RESTful API with ASP.NET Core 3 en [Pluralsight](https://app.pluralsight.com/library/courses/asp-dot-net-core-3-restful-api-building)
- Implementing Advanced RESTful Concerns with ASP.NET Core 3 en [Pluralsight](https://app.pluralsight.com/library/courses/asp-dot-net-core-3-advanced-restful-concerns)
